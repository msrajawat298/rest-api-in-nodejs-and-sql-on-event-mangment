const env = require("dotenv").config();
const express = require('express');
const app = express()
var db = require('./db/connection');
const bodyParser = require("body-parser");
// parse application/json
app.use(bodyParser.json());
const PORT = process.env.PORT;

/* Initally display all the records. */
app.get('/', function(request, response, next) {
    var sql = "SELECT * FROM event_details";
    db.query(sql, function(err, rows, fields) {
        if (err) {
            response.status(500).send({
                error: 'Something failed!'
            })
        }
        response.json(rows)
    })
});
/* Display Upcoming event records. */
app.get('/upcoming_event', function(request, response, next) {
    var sql = `SELECT * FROM event_details WHERE event_starting_time >= '${getCurrentDateTime()}'`;
    db.query(sql, function(err, rows, fields) {
        if (err) {
            response.status(500).send({
                error: 'Something failed!'
            })
        }
        if (Object.entries(rows).length !== 0) {
            response.json(rows)
        } else {
            response.json("Sorry :( No Upcoming Event Found...")
        }
    })
});
/* Display Live event records. */
app.get('/live_event/', function(request, response, next) {
    var sql = `SELECT * FROM event_details WHERE event_starting_time = '${getCurrentDateTime("datetime",10)}'`;
    console.log(sql);
    db.query(sql, function(err, rows, fields) {
        if (err) {
            response.status(500).send({
                error: 'Something failed!'
            })
        }
        if (Object.entries(rows).length !== 0) {
            response.json(rows)
        } else {
            response.json("Sorry :( No Live Event Found...")
        }
    })
});
/*post method for create product*/
app.post('/create_event', function(request, response, next) {
    var event_name = request.body.event_name;
    var event_duration = request.body.event_duration;
    var event_starting_time = request.body.event_starting_time;
    if (typeof event_name === "undefined" || typeof event_duration === "undefined" || typeof event_starting_time === "undefined") {
        return response.json({
            'status': 'fail',
            "error_message": "Please Provide Valid data",
            'result': "event_name,event_duration,event_starting_time All fields are required"
        })
    } else {
        var sql = `INSERT INTO event_details (event_name, event_starting_time, event_duration) VALUES ("${event_name}", "${event_starting_time}", "${event_duration}")`;
        db.query(sql, function(err, result) {
            if (err) {
                response.status(500).send({
                    error: 'Something failed!'
                })
            }
            response.json({
                'status': 'success',
                id: "Inserted Id is : " + result.insertId
            })
        })
    }

});
app.all('*', (request, response) => {
    response.json({
        'status': 'fail',
        "message": "unauthorize access!"
    })
})
app.listen(PORT, () => {
    console.log(`server is running at port no ${PORT}`);
})

function getCurrentDateTime(return_type = "datetime", less_time_In_Minute = 0) {
    var today = new Date();
    if (return_type == "datetime") {
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    }
    if (less_time_In_Minute != 0) {
        today.setMinutes(today.getMinutes() - less_time_In_Minute);
    }
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    return (return_type == "datetime") ? dateTime : time;
}